import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/administrar/:_id',
    name: 'Administrar',
    component: () => import('../views/AdminPoll.vue')
  },
  {
    path: '/respuestas/:poll_id',
    name: 'Respuestas',
    component: () => import('../views/AnswersReview.vue')
  },
  {
    path: '/aplicar/:_id',
    name: 'Responder',
    component: () => import('../views/Answer.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
