import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    api: axios.create({
      baseURL: 'http://localhost:4000/'
    })
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
