const express = require('express')
const app = express()
const morgan = require('morgan')
const cors = require('cors')

// Routes
const pollRoutes = require('./routes/poll')
const answerRoutes = require('./routes/answer')

// Configuraciones
app.set('port', process.env.PORT || 4000)


// Middlewares
app.use(cors({
    origin: '*'
}))
app.use(morgan('dev'))
app.use(express.json())

app.use('/poll', pollRoutes)
app.use('/answer', answerRoutes)

// Iniciando servidor
app.listen(app.get('port'), () => {
    console.log('Servidor corriendo en puerto '+app.get('port'))
})