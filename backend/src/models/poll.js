const mongoose = require('./../db/mongodb')

const Schema = mongoose.Schema

const Poll = mongoose.model('poll', new Schema({
    id: String,
    title: String,
    email: String,
    author: String,
    questions: Object
}))

module.exports = Poll