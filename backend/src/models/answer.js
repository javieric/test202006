const mongoose = require('../db/mongodb')

const Schema = mongoose.Schema

const Poll = mongoose.model('answer', new Schema({
    poll_id: String,
    answers: Object,
    answer_date: Date
}))

module.exports = Poll