const Answer = require('../models/answer')
const Poll = require('../models/poll')
const moment = require('moment')

const answerController = {
    create: async (req, res) => {
        const poll_id = req.body.poll_id
        const answers = req.body.answers
        const answer_date = moment.now()

        try {
            const request = await Answer.create({
                poll_id: poll_id,
                answers: answers,
                answer_date: answer_date
            })
            console.log(request)
            res.json({
                code: "SUCCESS"
            })
        } catch (err) {
            console.log(err)
            res.status(500).json({
                code: "ERROR_SERVER"
            })
        }
        
    },
    findByPoll: async (req, res) => {
        const poll_id = req.params.poll_id
        try {
            const pollRequest = await Poll.findOne({
                id: poll_id
            })

            const AnswerRequest = await Answer.find({
                poll_id: poll_id
            }).select({
                answers: 1,
                _id: 0
            })

            res.json({
                code: 'SUCCESS',
                poll: pollRequest,
                answers: AnswerRequest
            })
        } catch (err) {
            console.log(err)
            res.status(500).json({
                code: 'ERROR_SERVER'
            })
        }
    }
}

module.exports = answerController