const Poll = require('./../models/poll')
const moment = require('moment')

const pollController = {
    create: async (req, res) => {
        const title = req.body.title
        const author = req.body.author
        const email = req.body.email
        const questions = req.body.questions

        if (await pollController.titleExist(title) === true) {
            res.json({
                code: "ERROR_TITLE_ALREADY_EXIST"
            })
            return false;
        }

        try {
            const request = await Poll.create({
                id: moment.now(),
                title: title,
                author: author,
                email: email,
                questions: questions
            })

            console.log(request)
            res.json({
                code: "SUCCESS"
            })
            
        } catch (err) {
            console.log(err)
            res.status(500).json({
                code: "ERROR_SERVER"
            })
        }
        
    },
    find: async (req, res) => {
        try {
            const request = await Poll.find()
            res.json({
                code: 'SUCCESS',
                polls: request
            })
        } catch (err) {
            console.log(err)
            res.status(500).json({
                code: 'ERROR_SERVER'
            })
        }
    },
    findById: async (req, res) => {
        const id = req.params.id
        try {
            const request = await Poll.findById(id)
            res.json({
                code: 'SUCCESS',
                polls: request
            })
        } catch (err) {
            console.log(err)
            res.status(500).json({
                code: 'ERROR_SERVER'
            })
        }
    },
    titleExist: async (title) => {
        try {
            const request = await Poll.findOne({
                title: title
            })
            if (request !== null && request.title === title) {
                return true
            } else {
                return false
            }
        } catch (err) {
            console.log(err)
            return false
        }
    }
}

module.exports = pollController