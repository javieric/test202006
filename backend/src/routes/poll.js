const express = require('express')
const router = express.Router()
const pollController = require('../controllers/poll')

router.get('/', pollController.find)
router.get('/:id', pollController.findById)
router.put('/', pollController.create)

module.exports = router