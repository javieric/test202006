const express = require('express')
const router = express.Router()
const pollController = require('../controllers/answer')

router.put('/', pollController.create)
router.get('/:poll_id', pollController.findByPoll)

module.exports = router